# MOE GP Interactive
[![CircleCI](https://circleci.com/gh/Sproud-SG/moe-gp-interactive.svg?style=svg)](https://circleci.com/gh/Sproud-SG/moe-gp-interactive)

Interactive Learning Package for students in Junior Colleges for the Curriculum Planning and Development Division (CPDD).

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
Copy or move the following video files into the project directory. This is required because GitHub does not accept huge individual files being pushed to the repository.

See [README.md](https://github.com/Sproud-SG/MOE-GP-Interactive/assets/video/README.md) for the markdown formatted instructions on handling the video assets.

## Build Setup
```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:8080
$ npm run dev

# build for production with minification
$ npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Deployment
This Interactive Project has to be ran locally without both an internet connection and on the client side for security reasons as specified. We highly recommend to run this on the Intranet Network due to the technology stack having used. See Built With for the technology stack used.

Note: The video files are loaded asynchronously. We advise strongly not to open the `index.html` file locally. This can cause Cross-origin Resource sharing (CORS) error when used with the `file://` protocol. Thus, the Intranet must have the capability to run as a web server.

## Built With
* [Vue.js](https://vuejs.org/) - The Javascript Framework
* [Vue Router](https://github.com/vuejs/vue-router) - Vue Router for Single Page Application (SPA)
* [Vuex](https://vuex.vuejs.org/en/) - State management for Vue.js
* [Bootstrap v4](https://getbootstrap.com/) - The Front-end Framework

## Changelog
Details of changes for each release are documented in the [release notes](https://github.com/Sproud-SG/moe-gp-interactive/releases).

## Authors
* **Kun Lei Goh** - *Video Producer*
* **Lewis Chu** - *Project Liaising Officer*
* **Ru Chern Chong** - *Programmer* - [Github](https://github.com/ruchern)
* **Christina Lye** - *Visual Compositor*
* **Shafina Subari** - *Editor*
* **Mohd Ismail** - *Motion Graphic Designer*

See also the list of [contributors](https://github.com/Sproud-SG/MOE-GP-Interactive/contributors) who participated in this project.

## Acknowledgments
* Ministry of Education (MOE), Singapore
* [Sproud Private Limited](https://sproud.biz)
