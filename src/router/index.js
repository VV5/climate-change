import Vue from 'vue'
import Router from 'vue-router'
import Splash from '@/components/Splash'
import Scenario from '@/components/Scenario'
import Video from '@/components/Video'
import Choice from '@/components/Choice/Choice'
import Consequence from '@/components/Consequence'
import GeneratedReport from '@/components/GeneratedReport'
import Personality from '@/components/Personality'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Splash',
      component: Splash,
      meta: {
        title: 'Splash Screen'
      }
    },
    {
      path: '/scenario',
      name: 'Scenario',
      component: Scenario,
      meta: {
        title: 'Scenarios'
      }
    },
    {
      path: '/video/:id',
      name: 'Video',
      component: Video,
      meta: {
        title: 'Playing Scenario'
      }
    },
    {
      path: '/choice',
      name: 'Choice',
      component: Choice,
      meta: {
        title: 'Choice Selection'
      }
    },
    {
      path: '/consequence',
      name: 'Consequence',
      component: Consequence,
      meta: {
        title: 'Consequences'
      }
    },
    {
      path: '/report',
      name: 'Report',
      component: GeneratedReport,
      meta: {
        title: 'Report Card'
      }
    },
    {
      path: '/personality',
      name: 'Personality',
      component: Personality,
      meta: {
        title: 'Personality'
      }
    }
  ]
})
