import Vue from 'vue'
import App from '@/App'
import axios from 'axios'
import store from '@/store'
import router from '@/router'

Vue.config.productionTip = false

window.axios = axios
window.axios.defaults.headers.common = {
  'X-Requested-With': 'XMLHttpRequest'
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
