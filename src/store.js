import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    message: null,
    clickCount: 0,
    choiceSelectionCompleted: false,
    selectedScenes: [],
    selectedChoices: {
      scenes: [],
      choiceCount: 0,
      choiceAction: [],
      choiceFunding: []
    },
    generatedReport: [],
    scenes: [
      {
        id: 1,
        name: 'Director of Public Health (DPH)',
        question: 'Chancellor, would you like to wait for Dr. Helumi?'
      },
      {
        id: 2,
        name: 'Representative of Farmers Union',
        question: 'Chancellor, would you like to speak to the Minister for Agriculture?'
      },
      {
        id: 3,
        name: 'Secretariat of Palm Oil Smallholders',
        question: 'Chancellor, would you like to speak to the CEO of Hexapolis?'
      },
      {
        id: 4,
        name: 'Leaders of Non-governmental organisations',
        question: 'Chancellor, would you like to speak to the other NGO leaders?'
      },
      {
        id: 5,
        name: 'Leading scientists in Climate Engineering',
        question: 'Chancellor, would you like to speak to another scientist?'
      },
      {
        id: 6,
        name: 'Head of Tribes',
        question: 'Chancellor, would you like to listen to the others in the room?'
      }
    ],
    choices: {
      action: [
        {
          id: '1A',
          choice: 'Replace the Director of Public Health. She is too busy being a doctor and is not suited to be a director.',
          outcome: 'The Director of Public Health was replaced by a younger candidate.',
          consequences: {
            0: 'New director continued work of previous director. No impact.',
            1: 'New director introduced many new plans. Unable to manage PR because previous director was a well-loved figure. Public is angry.'
          },
          personality: 'Vision-driven'
        },
        {
          id: '2A',
          choice: 'Release the farmers. They are victims of the economic machinery. Their transgression is borne of necessity.',
          outcome: 'Farmers are released. Farmers\' Union is victorious.',
          consequences: {
            0: 'Farmers reunite with families. Human Rights Watch approves.',
            1: 'Farmers return to slash and burn farming.'
          },
          personality: 'People-centric'
        },
        {
          id: '3A',
          choice: 'Issue fines to businesses and revoke licenses. This will force business owners, both MNCs and SMEs, to take more responsibility.',
          outcome: 'Hundreds of companies have licenses revoked.',
          consequences: {
            0: 'Law suits and protests. Businesses go bust and higher unemployment.',
            1: 'Businesses are forced to innovate and newcomers emerge to replace the older ones.'
          },
          personality: 'Corporate-focused'
        },
        {
          id: '4A',
          choice: 'Regulate NGOs so that there can be more cohesiveness. Their agendas need to align with the Chancellor’s vision.',
          outcome: 'NGOs are confused. New central body is created to monitor and regulate activities and agenda of the organization.',
          consequences: {
            0: 'NGOs benefit from the structure and central organization. The NGOs work more cohesively and more is achieved.',
            1: 'NGOs go underground. Resources are spent arresting them. Intense competition among remaining NGOs.'
          },
          personality: 'Establishment-based'
        },
        {
          id: '6A',
          choice: 'Punish Tribes that do not adhere to agreement.',
          outcome: 'Enforcement teams come together from various Tribes to form Super-confederate Task Force.',
          consequences: {
            0: 'Task Force is respected. The Protocols are adhered. Super-confederate Task Force is lauded.',
            1: 'Super-confederate Task Force is seen as a nuisance. Tribes send their worst teams. There are heated arguments in Senate. The Task Force is ineffective.'
          },
          personality: 'Corporate-focused'
        }
      ],
      funding: [
        {
          id: '1F',
          choice: 'Increase funding to improve the Healthcare sector. The hospital currently seems to be understaffed.',
          outcome: '100 million pumped into Ministry of Healthcare.',
          consequences: {
            0: 'Opening of new hospitals to serve public / - Good for PR.',
            1: 'Higher cost causes heavy burden on the taxpayers.'
          },
          personality: 'Vision-driven'
        },
        {
          id: '2F',
          choice: 'Provide more funds to enable farmers to own cleaner and more advanced farming tools which are eco-friendly.',
          outcome: '$100 million funding taken from Confederacy Treasury. Farmers own  advanced tools.',
          consequences: {
            0: 'Improved yield and reduce amount of slash and burn',
            1: 'Farmers lacked expertise to use equipment and yielded poor harvests, which affected profitability.'
          },
          personality: 'Vision-driven'
        },
        {
          id: '3F',
          choice: 'Provide tax relief for businesses so that practice can be controlled centrally. Favouring SME interests would be counterproductive.',
          outcome: 'Signing of agreements and opening of new plants.',
          consequences: {
            0: 'Signing of agreements and opening of new plants – more jobs provided.  Because of easy control pollution is managed.',
            1: 'SMEs unable to sustain the competition. Feelings of discontent and betrayal affect popularity of Chancellor.  He loses the next election.'
          },
          personality: 'Establishment-based'
        },
        {
          id: '4F',
          choice: 'Provide funds to NGOs individually so that different approaches can be taken simultaneously.',
          outcome: 'NGOs receive financial aid with little restriction for use.',
          consequences: {
            0: 'More activists and activities aid the climate literacy initiatives which are diversified and reach different sectors.',
            1: 'With little control, funds are squandered on initiatives that yield insignificant results.'
          },
          personality: 'People-centric'
        },
        {
          id: '5F1',
          choice: 'Increase funding and enable current technology solutions to be accessible to more Tribes.',
          outcome: '$100 million is given to produce more solar panels for other Tribes.',
          consequences: {
            0: 'More Tribes have access to solar energy. Clean energy output is increased. However, climate issues remain.',
            1: 'Tribes squabble over the sources of the funds. Richer Tribes have no desire to help the poorer Tribes. The funding problem is a constant road block in Confederation Senate Session creating much tension.'
          },
          personality: 'People-centric'
        },
        {
          id: '5F2',
          choice: 'Increase funding for the research of more possible technological sources.',
          outcome: 'Newer research labs and production facility open to tackle climate change.',
          consequences: {
            0: 'New technologies successfully reduced temperature.',
            1: 'Space reflectors exploded due to unforeseen technical miscalculations.'
          },
          personality: 'Corporate-focused'
        },
        {
          id: '6F',
          choice: 'Initiate a Confederate Bank for multi-lateral funding with a governance structure headed by the best bankers.',
          outcome: '$100 million is given for Tribes to abide to the Confederate Protocol and Second Accord.',
          consequences: {
            0: 'Signing of more agreements. Tribes congratulate each other on their milestone agreements. Bilateral ties strengthen, enabling more trans-boundary undertakings.',
            1: 'Agreements remain as theoretical affirmations as Tribes find themselves unable to deliver on agreed terms because of complex internal issues.'
          },
          personality: 'Establishment-based'
        }
      ],
    },
    inProgress: false,
    downloadSpeed: 'Calculating...',
    loadingCompleted: false,
    resolution: 'high',
    currentScene: null
  },
  mutations: {
    toggleGameProgress (state) {
      state.inProgress = !state.inProgress
    },
    resetGameState (state) {
      state.message = null
      state.clickCount = 0
      state.choiceSelectionCompleted = false
      state.selectedScenes = []
      state.selectedChoices = {
        scenes: [],
        choiceCount: 0,
        choiceAction: [],
        choiceFunding: []
      }
      state.generatedReport = []
    },
    increaseClickCount (state) {
      state.clickCount++
    },
    setDownloadSpeed (state, speed) {
      state.downloadSpeed = speed
    },
    setLoadingCompletion (state, status) {
      state.loadingCompleted = status
    },
    setResolution (state, payload) {
      state.resolution = payload.resolution
    },
    setCurrentScene (state, payload) {
      state.currentScene = payload.id
    },
    setSelectedScenario (state, payload) {
      state.selectedScenes.push({
        id: payload.id,
        part: payload.part,
        name: state.scenes[payload.id - 1].name
      })
    },
    setSelection ({ selectedChoices }, payload) {
      switch (payload.type) {
        case 'action':
          if (!selectedChoices.choiceAction.includes(payload.id)) {
            selectedChoices.choiceAction.push(payload.id)
            selectedChoices.choiceCount++
          } else {
            const choiceIndex = selectedChoices.choiceAction.indexOf(payload.id)
            selectedChoices.choiceAction.splice(choiceIndex, 1)
            selectedChoices.choiceCount--
          }
          break
        case 'funding':
          if (!selectedChoices.choiceFunding.includes(payload.id)) {
            selectedChoices.choiceFunding.push(payload.id)
            selectedChoices.choiceCount++
          } else {
            const choiceIndex = selectedChoices.choiceFunding.indexOf(payload.id)
            selectedChoices.choiceFunding.splice(choiceIndex, 1)
            selectedChoices.choiceCount--
          }
          break
        default:
          break
      }
    },
    setChoiceCompleted (state) {
      state.choiceSelectionCompleted = true
    },
    undoChoices (state) {
      state.choiceSelectionCompleted = false
    },
    setMessage ({ message }, payload) {
      console.log(payload)
      message = payload.message
    },
    generateConsequences (state) {
      const selectedChoices = state.selectedChoices.choiceAction.concat(state.selectedChoices.choiceFunding)
      const choiceAction = state.choices.action
      const choiceFunding = state.choices.funding

      let generatedReport, consequenceIndex

      choiceAction.forEach(choice => {
        if (selectedChoices.includes(choice.id)) {
          consequenceIndex = Math.floor(Math.random() * 2)

          generatedReport = {
            id: choice.id,
            choice: choice.choice,
            outcome: choice.outcome,
            consequence: {
              id: consequenceIndex,
              description: choice.consequences[consequenceIndex]
            },
            personality: choice.personality
          }

          state.generatedReport.push(generatedReport)
        }
      })

      choiceFunding.forEach(choice => {
        if (selectedChoices.includes(choice.id)) {
          consequenceIndex = Math.floor(Math.random() * 2)

          generatedReport = {
            id: choice.id,
            choice: choice.choice,
            outcome: choice.outcome,
            consequence: {
              id: consequenceIndex,
              description: choice.consequences[consequenceIndex]
            },
            personality: choice.personality
          }

          state.generatedReport.push(generatedReport)
        }
      })
    }
  }
})
