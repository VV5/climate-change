# Deploying of video assets
This is important to ensure that the videos are able to load properly upon deployment of the product.

## Naming convention
To ensure that the videos are properly loaded based on how the code is written.

`video-X-Y.mp4`

Where X is the index number (1, 2, 3, et al) and Y is the Part of the video (A or B).

Example:
```
video-1-A.mp4 - Hospital Video Clip Part A
video-6-B.mp4 - Head of Tribes Video Clip Part B
```

The table below describes the structure of video assets:

| Scenario | Index Number | Parts |
| -------- | ------------ | ----- |
| Hospital | 1 | A & B |
| Farmers Union | 2 | A & B |
| Palm Oil Representatives | 3 | A & B |
| NGOs | 4 | A & B |
| Research Facility | 5 | A & B |
| Head of Tribes | 6 | A & B |

