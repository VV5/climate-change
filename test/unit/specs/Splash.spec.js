import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Splash from '@/components/Splash'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Splash', () => {
  let wrapper, store, mutations

  mutations = {
    toggleGameProgress: jest.fn()
  }

  store = new Vuex.Store({
    mutations
  })

  beforeEach(() => {
    wrapper = shallowMount(Splash, {
      store, localVue
    })
  })

  test('should start the game when button is clicked', () => {
    const router = new VueRouter()

    wrapper = shallowMount(Splash, {
      store, router, localVue
    })

    wrapper.find('.btn-play').trigger('click')

    expect(mutations.toggleGameProgress).toHaveBeenCalled()
  })
})
