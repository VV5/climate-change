# Changelog

## Unreleased
- Removed loading screen due to an unknown behaviour
- Upgraded backend to support relative path
